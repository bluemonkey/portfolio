#include <Arduino.h>
#include <stdint.h>
#include <ts_lcd.h>

#define SELECT_REGISTER_DATA 1
#define SELECT_REGISTER_INSTRUCTION 0
#define OPERATION_READ 1
#define OPERATION_WRITE 0
#define BYTE_LENGTH 8

// blue pill pins
uint8_t RS;
uint8_t RW; 
uint8_t E;
uint8_t dataPins[BYTE_LENGTH];

// static interface
static void dataPinWrite(const uint8_t pin, const bool data);
static void disable();
static void enable(bool rs, bool rw);
static uint8_t countStringLength(const char* string);
static bool lcdIsBusy();
static void lcdCommand(bool rs, bool rw, uint8_t cmd);

// static implementations
static void dataPinWrite(const uint8_t pin, const bool data){
  pinMode(pin, OUTPUT);
  digitalWrite(pin, data);
}

static void disable(){
	digitalWrite(E, 0); 
}

static void enable(bool rs, bool rw){
	digitalWrite(RS, rs);
  digitalWrite(RW, rw);
  digitalWrite(E, 1);
}

static uint8_t countStringLength(const char* string){
	uint8_t i = 0;
	while(string[i] != '\0') i++;
	return i;
}

static bool lcdIsBusy(){	
	delayMicroseconds(80);
	enable(0, 1);
	pinMode(dataPins[0], INPUT);
	bool isBusy = digitalRead(dataPins[0]);
  disable();
	return isBusy;
}

static void lcdCommand(bool rs, bool rw, uint8_t cmd){
	while(lcdIsBusy());
	enable(rs, rw);
	for(uint8_t i = 0; i < BYTE_LENGTH; ++i){
		dataPinWrite(dataPins[i], cmd >> 7-i & 1);
	}	
  disable();
}

void lcdSetPins(uint8_t rs, uint8_t rw, uint8_t e, uint8_t dp7, uint8_t dp6, uint8_t dp5, uint8_t dp4,
						 		uint8_t dp3, uint8_t dp2, uint8_t dp1, uint8_t dp0){
	RS = rs;
	RW = rw;
	E = e;
	dataPins[0] = dp7;
	dataPins[1] = dp6;
	dataPins[2] = dp5;
	dataPins[3] = dp4;
	dataPins[4] = dp3;
	dataPins[5] = dp2;
	dataPins[6] = dp1;
	dataPins[7] = dp0;
	
	pinMode(RS, OUTPUT);
  pinMode(RW, OUTPUT);
  pinMode(E,  OUTPUT);
  for(uint8_t i = 0; i < BYTE_LENGTH; ++i)
  	pinMode(dataPins[i], OUTPUT);	
}

void lcdWriteChar(uint8_t line, uint8_t position, char character){	
	uint8_t addressDDRAM = 128;

	if(line == 1){
		if(position < 0)
			position = 0;

		else if(position > 15)
			position = 15;		

		addressDDRAM += position;
	} 

	else if(line == 2){
		if(position < 0)
			position = 64;

		else if (position > 15)
			position = 79;

		else
			position += 64;

		addressDDRAM += position;		
	}

	lcdCommand(0, 0, addressDDRAM);	
	if(character < 32)
		character = 32;

	lcdCommand(1, 0, character);
}

void lcdWriteLine(uint8_t line, uint8_t position, const char* string){
	uint8_t stringLength = countStringLength(string);

	if(stringLength > 16)
		stringLength = 16;

	for(uint8_t i = 0, j = position; i < j; ++i){
		lcdWriteChar(line, i, ' ');
	}	

	for(uint8_t i = position, j = stringLength+position > 16 ? 16 : stringLength+position; i < j; ++i){
		lcdWriteChar(line, i, string[i-position]);
	}
	
	for(uint8_t i = stringLength+position, j = 16; i < j; ++i){
		lcdWriteChar(line, i, ' ');
	}
}

void lcdDisplayClear(){
	lcdCommand(SELECT_REGISTER_INSTRUCTION, OPERATION_WRITE, 1);
	delayMicroseconds(1520);
}

void lcdCursorOrDisplayShift(bool shiftee_cursor0_or_display1, bool shift_direction_left0_right1){
	uint8_t cursorOrDisplayShift = 16;
	cursorOrDisplayShift |= shiftee_cursor0_or_display1 << 3;
	cursorOrDisplayShift |= shift_direction_left0_right1 << 2;
	lcdCommand(SELECT_REGISTER_INSTRUCTION, OPERATION_WRITE, cursorOrDisplayShift);
}

void lcdFunctionSet(bool dataLineCount, bool displayLineCount, bool fontSize){
	uint8_t functionSet = 32;
	functionSet |= dataLineCount << 4;
	functionSet |= displayLineCount << 3;
	functionSet |= fontSize << 2;	
	lcdCommand(SELECT_REGISTER_INSTRUCTION, OPERATION_WRITE, functionSet);
}

void lcdDisplayOnOff(bool display, bool cursor, bool cursor_position){
	uint8_t displayOnOff = 8;
	displayOnOff |= display << 2;
	displayOnOff |= cursor << 1;
	displayOnOff |= cursor_position << 0;
	lcdCommand(SELECT_REGISTER_INSTRUCTION, OPERATION_WRITE, displayOnOff);
}

void lcdEntryModeSet(bool shiftee_cursor0_or_display1, bool shift_direction_left0_right1){
	uint8_t entryModeSet = 4;
	if(shiftee_cursor0_or_display1)
		shift_direction_left0_right1 = !shift_direction_left0_right1;

	entryModeSet |= shift_direction_left0_right1 << 1;
	entryModeSet |= shiftee_cursor0_or_display1 << 0;
	lcdCommand(SELECT_REGISTER_INSTRUCTION, OPERATION_WRITE, entryModeSet);
}
