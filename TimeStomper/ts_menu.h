#ifndef IG_MENU_H
#define IG_MENU_H
#include <stdint.h>

char getButtonPress();

void timeOfArrival();
void timeOfDeparture();
void accumulatedTime();
void adminConfig();

#endif