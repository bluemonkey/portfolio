#ifndef IG_CALENDAR_H
#define IG_CALENDAR_H
#include <stdint.h>

extern const char * const WEEK_DAY[];
extern const uint16_t END_OF_MONTH[];
extern const uint16_t FIFTY_THIRD_WEEK[];
extern const uint32_t SECONDS_PER_DAY;
extern const uint32_t SECONDS_PER_HOUR; 
extern const uint16_t EPOCH_YEAR;
extern const uint16_t DAYS_PER_BUNDLE;
extern const uint16_t DAYS_PER_BLOCK_NORMAL; 
extern const uint16_t DAYS_PER_YEAR_LEAP;
extern const uint16_t DAYS_PER_YEAR_NORMAL;
extern const uint8_t 	MINUTES_PER_HOUR;
extern const uint8_t 	SECONDS_PER_MINUTE;
extern const uint8_t 	WEEKS_PER_YEAR;
extern const uint8_t 	HOURS_PER_DAY;
extern const uint8_t 	MONTHS_PER_YEAR;
extern const uint8_t 	EPOCH_WEEK_NUMBER;
extern const uint8_t  END_OF_DECEMBER;
extern const uint8_t	END_OF_SEPTEMBER;
extern const uint8_t 	LAST_WEEK_DAY;
extern const uint8_t 	YEARS_PER_BUNDLE;
extern const uint8_t 	YEARS_PER_BLOCK_NORMAL;
extern const uint8_t	EPOCH_MONTH;
extern const uint8_t 	MONTH_OFFSET;
extern const uint8_t  DAYLIGHT_START_HOUR;
extern const uint8_t	EPOCH_WEEK_DAY;
extern const uint8_t	ZEROTH_DAY;

typedef struct {
	uint16_t year;
	uint8_t month;
	uint8_t day;
} YearMonthDay;

typedef struct {
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
} HourMinuteSecond;

typedef struct {
	uint8_t number;
	uint8_t day;
} Week;

typedef struct {
	YearMonthDay ymd;
	HourMinuteSecond hms;
	Week week;
} Date;

Date getDate(uint32_t seconds_since);
Date makeDate(uint16_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t minute, uint8_t second);
//void viewDate(Date date);
uint32_t convertDateToSeconds(Date date);
uint32_t exactConvert(Date date, bool repeating_hour);
uint8_t daysPerMonth(uint16_t year, uint8_t month, uint8_t day);
#endif