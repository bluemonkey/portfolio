#include <cstdio>
#include <cstring>
#include <cstdlib>
#include "ts_calendar.h"

void viewDate(const Date* const date){
	printf("Year: %hu\n", date->ymd.year);
	printf("Month: %hu\n", date->ymd.month);
	printf("Day: %hu\n", date->ymd.day);

	printf("Hour: %hu\n", date->hms.hour);
	printf("Minute: %hu\n", date->hms.minute);
	printf("Second: %hu\n", date->hms.second);

	printf("Week.number: %hu\n", date->week.number);
	printf("Week.day: %hu\n\n", date->week.day);
}

long cstringToLong(const char* const str){
	return strtol(str, NULL, 10);
}

#define MAKEDATE 8
#define GETDATE 3
#define CMD_COMMAND 1
#define FIRST_PARAM 2

#define MAKEDATE_PARAMS_LENGTH 6
#define GETDATE_PARAMS_LENGTH 1

void usageMessage(){
		printf("usage:\n");
		printf("-makedate <year> <month> <day> <hour> <minute> <second>\n");
		printf("-getdate <seconds>\n");
}

void debugViewParsedArguments(const long* const arguments, int argc){
	if(argc == MAKEDATE){
		for(unsigned char i = 0; i < MAKEDATE_PARAMS_LENGTH; ++i)
			printf("arguments[%hu]: %lu\n", i, arguments[i]);
	}
	else if(argc == GETDATE){
		printf("arguments[%hu]: %lu\n", 0, arguments[0]);
	}
}

void executeCommand(int argc, char** argv)
{
	if(argc == 1 || argc == 2 || (argc > 2 && strcmp(argv[CMD_COMMAND], "-makedate") && strcmp(argv[CMD_COMMAND], "-getdate"))){
		usageMessage();
	}

	else if(!strcmp(argv[CMD_COMMAND], "-makedate") && argc == MAKEDATE){
		Date today = makeDate(cstringToLong(argv[FIRST_PARAM]), 
													cstringToLong(argv[FIRST_PARAM+1]), 
													cstringToLong(argv[FIRST_PARAM+2]), 
													cstringToLong(argv[FIRST_PARAM+3]), 
													cstringToLong(argv[FIRST_PARAM+4]), 
													cstringToLong(argv[FIRST_PARAM+5]));
		printf("MAKEDATE:");
		viewDate(&today);
		printf("convertDateToSeconds(): %u\n\n", convertDateToSeconds(today));
	}
	
	else if(!strcmp(argv[CMD_COMMAND], "-getdate") && argc == GETDATE){
		Date date = getDate(cstringToLong(argv[FIRST_PARAM]));
		printf("GETDATE:");
		viewDate(&date);
	}
}

int main(int argc, char** argv){
	executeCommand(argc, argv);
	return 0;
}

/*
Date makeDate(uint16_t year,
							uint8_t month, 
							uint8_t day, 
							uint8_t hour, 
							uint8_t minute, 
							uint8_t second);
*/

/*
typedef struct {
	uint16_t year;
	uint8_t month;
	uint8_t day;
} YearMonthDay;

typedef struct {
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
} HourMinuteSecond;

typedef struct {
	uint8_t number;
	uint8_t day;
} Week;

typedef struct {
	YearMonthDay ymd;
	HourMinuteSecond hms;
	Week week;
} Date;
*/
