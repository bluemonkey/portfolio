#include <stdint.h>
#include "ts_calendar.h"

#define DECEMBER 12
#define NOVEMBER 11
#define OCTOBER 10
#define SEPTEMBER 9
#define AUGUST 8
#define JULY 7
#define JUNE 6
#define MAY 5
#define APRIL 4
#define MARCH 3
#define FEBRUARY 2
#define JANUARY 1

#define DAYS_PER_WEEK 7

const char * const WEEK_DAY[]						= {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
const uint16_t  END_OF_MONTH[] 					= {31, 61, 92, 122, 153, 184,	214, 245,	275, 306,	337, 366}; // March..February
const uint16_t  FIFTY_THIRD_WEEK[] 			= {261, 574, 887, 1148, 1461}; // nth weeks have week number 53
const uint32_t 	SECONDS_PER_DAY 				= 86400;
const uint32_t 	SECONDS_PER_HOUR 				= 3600; 
const uint16_t 	EPOCH_YEAR 							= 2016;
const uint16_t 	DAYS_PER_BUNDLE 				= 1461;
const uint16_t 	DAYS_PER_BLOCK_NORMAL 	= 1095;
const uint16_t 	DAYS_PER_YEAR_LEAP 			= 366;
const uint16_t 	DAYS_PER_YEAR_NORMAL 		= 365;
const uint8_t 	SECONDS_PER_MINUTE 			= 60;
const uint8_t 	MINUTES_PER_HOUR 				= 60;
const uint8_t 	WEEKS_PER_YEAR 					= 52;
const uint8_t 	HOURS_PER_DAY 					= 24;
const uint8_t 	MONTHS_PER_YEAR 				= 12;
const uint8_t 	EPOCH_WEEK_NUMBER 			= 9;
const uint8_t 	END_OF_DECEMBER					= 9;
const uint8_t 	END_OF_SEPTEMBER				= 6;
const uint8_t 	LAST_WEEK_DAY						= 6;
const uint8_t 	YEARS_PER_BUNDLE 				= 4;
const uint8_t 	YEARS_PER_BLOCK_NORMAL 	= 3;
const uint8_t 	EPOCH_MONTH 						= 3;
const uint8_t 	MONTH_OFFSET 						= 3;
const uint8_t   DAYLIGHT_START_HOUR			= 3;
const uint8_t		EPOCH_WEEK_DAY					= 1;
const uint8_t		ZEROTH_DAY						 	= 1;

static YearMonthDay getYearMonthDay(uint16_t days);
static Week getWeek(uint16_t days);
static uint16_t convertYMDtoDays(uint16_t year, uint8_t month, uint8_t day);
static uint16_t convertYearsToDays(uint16_t year, uint8_t month);
static uint16_t convertMonthToDays(uint8_t month);
static uint8_t handleMarch(Date date, uint8_t forward_march);
static uint8_t handleOctober(Date date, uint32_t seconds_since, uint8_t backward_october);
static uint8_t getForwardClock(Date date, uint32_t seconds_since);
static uint8_t getWeekNumber(uint16_t weeks);
static uint8_t getSummerTimeBorderDay(uint16_t year, uint8_t month);

/*******************getYearMonthDay*******************************************************************
General:	Calculates the year, the month and the day of month since epoch 2016/03/01 up to 2100/02/28. 
					(YYYY/MM/DD)
Accepts:	days:	The number of days since epoch.
Returns:  A <YearMonthDay> structure with year, month and day.
**************************************************/
static YearMonthDay getYearMonthDay(uint16_t days){
	YearMonthDay ymd;
	uint16_t remainder; 																										// 0..365
	uint16_t stub = days % DAYS_PER_BUNDLE; 																// 0..1460
	uint16_t base = EPOCH_YEAR + days / DAYS_PER_BUNDLE * YEARS_PER_BUNDLE; // 2016..2100
	
	if(stub == 0){
		ymd.year = base;
		remainder = 0;
	}

	else if(stub <= DAYS_PER_BLOCK_NORMAL){ 				 // 0..1095
		ymd.year = base + stub / DAYS_PER_YEAR_NORMAL; // 2016..2100 + 0..3
		remainder = stub % DAYS_PER_YEAR_NORMAL; 			 // 0..364
	}

	else if(stub > DAYS_PER_BLOCK_NORMAL){														 // 1096..1460
		ymd.year = base + YEARS_PER_BLOCK_NORMAL;												 // 2016..2100 + 3
		remainder = (stub - DAYS_PER_BLOCK_NORMAL) % DAYS_PER_YEAR_LEAP; // 1..365
	}

	if(remainder + ZEROTH_DAY > END_OF_MONTH[END_OF_DECEMBER]){		
		ymd.year++;
	}
	
	for(uint8_t i = 0; i < MONTHS_PER_YEAR; ++i){
		if(ZEROTH_DAY + remainder <= END_OF_MONTH[i]){
			ymd.month = i + 3; 
			if(ymd.month > MONTHS_PER_YEAR){
				ymd.month %= MONTHS_PER_YEAR;
			}			
			if(i == 0){
				ymd.day = ZEROTH_DAY + remainder;
			}
			else {
				ymd.day = ZEROTH_DAY + remainder - END_OF_MONTH[i - 1];
			}
			break;
		}
	}	

	return ymd;
}

/***********getWeek**********************************************************************
General: Calculates the weekday and fetches the week number by calling the getWeekNumber.				 
Accepts: Number of days since epoch.
Returns: A <Week> structure with weekday and week number.
************getWeek***************/
static Week getWeek(uint16_t days){
	uint16_t weeks = days / DAYS_PER_WEEK;
	uint8_t week_day = days % DAYS_PER_WEEK;
	uint16_t epoch_adjusted = weeks + EPOCH_WEEK_NUMBER;
	uint8_t week_number;

	if(week_day + EPOCH_WEEK_DAY > LAST_WEEK_DAY)
		epoch_adjusted++;	
	
	week_number = getWeekNumber(epoch_adjusted);
	Week week;
	week.number = week_number;
	week.day = (days + EPOCH_WEEK_DAY) % DAYS_PER_WEEK; // week day 0..6

	return week;
}

/***************convertYMDtoDays***************************************************************
General:	Calculates the total number of days from a given year, a month, and the day of month.
Accepts:	year:  The year of the inspected date (2016-2100)
					month: The month of the inspected date(1-12)
					day:   The day of month of the inspected date (1-31)
Returns:	The total number of days from the given inputs. (1-7670)
****************convertYMDtoDays*******************************************/
static uint16_t convertYMDtoDays(uint16_t year, uint8_t month, uint8_t day){
	uint16_t month_to_days = convertMonthToDays(month);
	uint16_t years_to_days = convertYearsToDays(year, month);
	uint16_t days_since = month_to_days + years_to_days + day - ZEROTH_DAY;
	return days_since;
}

/***************convertYearsToDays****************************************************
General:	Calculates the amount of days for full years. Takes into account leap years.
Used by: 	convertYMDtoDays, a function that converts a <Date> structure into days.
Accepts:	year:  The year of the inspected date. (2016-2100)
					month: The month of the inspected date. (1-12) Month is required because the epoch
								 date begins on March the 1st 2016 meaning the first year is a stub year.
Returns:	The total number of days for full years since epoch date 2016 March the 1st.
****************convertYearsToDays******************************/
static uint16_t convertYearsToDays(uint16_t year, uint8_t month){	
	uint8_t delta = year - EPOCH_YEAR;
	if(delta > 0){
		if(month < MARCH){			
			if(delta - 1 == 0)
				return 0;
			return (delta - 1) * DAYS_PER_YEAR_NORMAL + (delta - 1) / YEARS_PER_BUNDLE;
		}
		if(month >= MARCH)
			return delta * DAYS_PER_YEAR_NORMAL + delta / YEARS_PER_BUNDLE;
	}
	return 0;
}

/***************convertMonthToDays**********************************************************
General:	Calculates the amount of days since the beginning of March 1st up to the beginning
					of the given month. E.g. month=4 (April) returns 31, because March has 31 days that
					is already past.
Used by: convertYMDtoDays, a function that converts a <Date> structure into days.
Accepts: month: A month between 1-12. (from January to December)
Returns: The cumulative number of days from prior months up to the beginning of given month.
				 A value in the range of 31-337. (from January to November)
****************convertMonthToDays***************/
static uint16_t convertMonthToDays(uint8_t month){
	if(month == MARCH) return 0;	
	return END_OF_MONTH[(month + MONTHS_PER_YEAR - MONTH_OFFSET - 1) % MONTHS_PER_YEAR];
}


/**************getWeekNumber********************
General: Calculates the week number for getWeek.
Accepts: Number of 7-day periods (weeks) since epoch.
Returns: Calendar week number. 
***************getWeekNumber****************/
static uint8_t getWeekNumber(uint16_t weeks){
	uint8_t week_number;
	uint8_t k;
	weeks %= FIFTY_THIRD_WEEK[4];

	for(uint8_t i = 0, j = 5; i < j; ++i)
	{
		if(weeks == FIFTY_THIRD_WEEK[i])
			return 53;
		
		if(weeks < FIFTY_THIRD_WEEK[i])
		{
			k = i;
			break;
		}
	}

	weeks -= k;
	week_number = weeks % 52;

	if(week_number == 0)
		return 52;
	
	return week_number;
}

/**************getForwardClock**************************************************************************
General:	Determines if the date.hms.hour field of <Date> structure should be forwarded by 1 hour due to
					daylight savings.
Accepts:	date: 					A copy of the inspected <Date> structure.
					seconds_since:	Seconds since epoch is required by another function this function calls.
Returns:	Returns either 0 or 1, indicating if the clock should be forwarded by 1 hour or not.
***************getForwardClock***********************************/
static uint8_t getForwardClock(Date date, uint32_t seconds_since){
	uint8_t forward_march	= getSummerTimeBorderDay(date.ymd.year, MARCH);
	uint8_t backward_october	= getSummerTimeBorderDay(date.ymd.year, OCTOBER);

	if(date.ymd.month > 3 && date.ymd.month < 10)
		return 1;

	if(date.ymd.month == 3)
		return handleMarch(date, forward_march);

	if(date.ymd.month == 10)
		return handleOctober(date, seconds_since, backward_october);
	
	return 0;
}

/**************handleMarch*******************************************************************************
General:	Figures out if the date and its hour is past the summer time starting boundary i.e. at 03:00 am
					on last Sunday in March. This function is called by getForwardClock.
Accepts: 	date:					 A <Date> structure for the inspected date.
					forward_march: The day of March on which the summer time starting boundary is potentially being 
												 crossed. forward_march can be fetched by calling getSummerTimeBorderDay.
Returns:	Returns either 0 or 1 indicating if the clock should be forwarded due to boundary crossing.
					0="no forward", 1="forward".
***************handleMarch**********************************/
static uint8_t handleMarch(Date date, uint8_t forward_march){	
	if(date.ymd.day < forward_march)
		return 0;

	if(date.ymd.day == forward_march && date.hms.hour < 3)
		return 0;	

	return 1;
}

/**************handleOctober*******************************************************************************
General:	Same as handleMarch except figures out the date and its hour past the summer time ending boundary
					which happens at 04:00 am on last Sunday in October. This function is called by getForwardClock.
Accepts:	date: 						A copy of the inspected <Date> structure.
					seconds_since: 		Seconds since epoch is required because we do not know if the date.hms.hour of
														<Date> structure is already being forwarded by 1 hour or not.
					backward_october:	The day of October on which the summer time ending boundary is potentially being
														crossed. backward_march can be fetched by calling getSummerTimeBorderDay.
Returns:  Returns either 0 or 1, indicating if the clock should be forwarded due to staying inside the
					boundaries of summer time. 0="no forward", 1="forward"
***************handleOctober*************************************************************/
static uint8_t handleOctober(Date date, uint32_t seconds_since, uint8_t backward_october){	
	if(date.ymd.day < backward_october)
		return 1;
	
	if(date.ymd.day == backward_october){
		if(seconds_since % SECONDS_PER_DAY / SECONDS_PER_HOUR < 3)
			return 1;		
	}
	return 0;
}

/**************getSummerTimeBorderDay******************************************
General:	Finds the day of month for the last Sunday on a given month and year.
Accepts:	year: any year betwen (2016-2100)
					month: 3=March, 10=October
Returns:  Day of month (25-31)
***************getSummerTimeBorderDay******************************/
static uint8_t getSummerTimeBorderDay(uint16_t year, uint8_t month){	
	uint8_t LAST_SUNDAY[DAYS_PER_WEEK];
	uint8_t delta = year - EPOCH_YEAR;
	uint8_t length = DAYS_PER_WEEK;

 	if(month == MARCH){
		LAST_SUNDAY[0] = 27;
		LAST_SUNDAY[1] = 26;
		LAST_SUNDAY[2] = 25;
		LAST_SUNDAY[3] = 31;
		LAST_SUNDAY[4] = 30;
		LAST_SUNDAY[5] = 29;
		LAST_SUNDAY[6] = 28;

 	}
	else if(month == OCTOBER){
		LAST_SUNDAY[0] = 30;
		LAST_SUNDAY[1] = 29;
		LAST_SUNDAY[2] = 28;
		LAST_SUNDAY[3] = 27;
		LAST_SUNDAY[4] = 26;
		LAST_SUNDAY[5] = 25;
		LAST_SUNDAY[6] = 31;
	}
	else 
		return 0;	

	uint8_t double_shifts = delta / YEARS_PER_BUNDLE;
	uint8_t shifts 				= delta - double_shifts;
	uint8_t long_index 		= double_shifts * 2 + shifts;
	uint8_t index 				= long_index % length;

	return LAST_SUNDAY[index];
}


/****getDate***********************************************************
General: Converts seconds since epoch into meaningful <Date> structure.
Accepts: A number of seconds since epoch. Either user input or counted by a timer.
Returns: A <Date> structure representing the time since Epoch 2016 March the 1st. 
*****getDate************************/
Date getDate(uint32_t seconds_since){	
	uint32_t minutes_since = seconds_since / SECONDS_PER_MINUTE;
	uint32_t hours_since = minutes_since / MINUTES_PER_HOUR;
	uint16_t days_since = hours_since / HOURS_PER_DAY;
	uint8_t forward_clock;

	HourMinuteSecond hms;
	hms.hour = hours_since % HOURS_PER_DAY;
	hms.minute = minutes_since % MINUTES_PER_HOUR;
	hms.second = seconds_since % SECONDS_PER_MINUTE;

	Date date;
	date.ymd = getYearMonthDay(days_since); 
	date.week = getWeek(days_since); 				
	date.hms = hms;	

	forward_clock	= getForwardClock(date, seconds_since);

	if(forward_clock){
		hours_since++;
		if(hours_since % HOURS_PER_DAY == 0){
			days_since++;
			date.ymd = getYearMonthDay(days_since);
			date.week = getWeek(days_since);
		}
		date.hms.hour = hours_since % HOURS_PER_DAY;
	}
	return date;
}

/****makeDate**********************************************************
General:	Converts a user input date information into <Date> structure.
Accepts:  year:   A year from 2016 to 2100.
					month:  A month from 1 to 12.
					day:    A day of month from 1 - 31.
					hour:   An hour of a day from 0 to 23.
					minute: A minute of an hour from 0 to 59
					second: A second of a minute from 0 to 59 
NOTE: 		Forces all invalid inputs into valid ones, silently.
Returns: A valid <Date> structure.
*****makeDate**********************************************/
Date makeDate(uint16_t year, uint8_t month, uint8_t day, 
							uint8_t hour, uint8_t minute, uint8_t second){
	Date date;
	Week week;

	if(year < 2016) year = 2016;	
	if(year > 2100) year = 2100;	

	if(year == 2016 && month < 3) month = 3;
	if(year == 2100 && month > 2) month = 2;
	if(month < 1) month = 1;	
	if(month > 12) month = 12;

	if(day < 1) day = 1;	
	if(day > 28) day = daysPerMonth(year, month, day);	

	uint16_t days_since = convertYMDtoDays(year, month, day);
	week = getWeek(days_since);
	
	if(hour > 23) hour = 23;	
	if(minute > 59) minute = 59;
	if(second > 59) second = 59;	
	
	date.ymd.year = year;
	date.ymd.month = month;
	date.ymd.day = day;
	date.hms.hour = hour;
	date.hms.minute = minute;
	date.hms.second = second;
	date.week = week;
	return date;
}

/********convertDateToSeconds*********************************
General: Converts a <Date> structure into seconds since epoch.
Accepts: date: A <Date> structure.
Returns: The total number of seconds since epoch.
*********convertDateToSeconds***********/
uint32_t convertDateToSeconds(Date date){	
	uint32_t seconds_since_0 = exactConvert(date, 0);
	uint32_t seconds_since_1 = exactConvert(date, 1);
	Date date_0 = getDate(seconds_since_0);
	Date date_1 = getDate(seconds_since_1);
	return date.hms.hour == date_0.hms.hour ? exactConvert(date_0, 0) : exactConvert(date_1, 1);	
}

/********exactConvert***************************************************************************
General: As summer time ends there is a two hour period during which the same hour is run twice.
				 For example: on 30.10.2018 at 03:00 - 04:00, an hour from 3 to 4am is run twice in
				 succession, making it ambiguous which hour is in question. With this function, user may
				 indicate which hour is in question.				 
NOTE:		 When creating a <Date> structure manually, the hour is set to the first occurence of the
				 repeating hour.
Accepts: date: <Date> structure
				 repeating_hour: 0=1st occurence, 1=2nd occurence
Returns: seconds since epoch
*********exactConvert********************************/
uint32_t exactConvert(Date date, bool repeating_hour){	
	uint16_t days_since = convertYMDtoDays(date.ymd.year, date.ymd.month, date.ymd.day);
	uint32_t hours_since = days_since * HOURS_PER_DAY + date.hms.hour;	
	if(repeating_hour){
		hours_since--;
	}	
	return (uint32_t)hours_since * SECONDS_PER_HOUR + date.hms.minute * SECONDS_PER_MINUTE + date.hms.second;
}

/*******daysPerMonth*********************************************************************
General:	Calculates the maximum amount of days a given month has, depending on the year.
Accepts:	year:  A year between 2016-2100.
					month: A month between 1-12. (from January to December)
Returns:	The maximum number of days in the given month.
********daysPerMonth******************************/
uint8_t daysPerMonth(uint16_t year, uint8_t month, uint8_t day){
	switch(month){
		case JANUARY:
		case MARCH:
		case MAY:
		case JULY:
		case AUGUST:
		case OCTOBER:
		case DECEMBER:
		if(day <= 31){
			return day;
		}
		else
			return 31;

		case FEBRUARY:
		return year % YEARS_PER_BUNDLE == 0 ? 29 : 28;
		
		case APRIL:
		case JUNE:
		case SEPTEMBER:
		case NOVEMBER:
		if(day <= 30){
			return day;	
		}
		else
			return 30;
	}
	return 0;
}