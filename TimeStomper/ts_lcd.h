#ifndef IG_LCD_H
#define IG_LCD_H
#include <stdint.h>

void lcdSetPins(uint8_t rs, uint8_t rw, uint8_t e, uint8_t dp7, uint8_t dp6, uint8_t dp5, uint8_t dp4,
						 			 uint8_t dp3, uint8_t dp2, uint8_t dp1, uint8_t dp0);

void lcdWriteChar(uint8_t line, uint8_t position, char character);
void lcdWriteLine(uint8_t line, uint8_t position, const char* string);

void lcdDisplayClear();

#define BITS_EIGHT 1
#define BITS_FOUR 0
#define LINES_TWO 1
#define LINES_ONE 0
#define FONT_5X11 1
#define FONT_5X8 0
void lcdFunctionSet(bool dataLineCount, bool displayLineCount, bool fontSize);

#define DISPLAY_ON 1
#define DISPLAY_OFF 0
#define CURSOR_ON 1
#define CURSOR_OFF 0
#define CURSOR_POSITION_ON 1
#define CURSOR_POSITION_OFF 0
void lcdDisplayOnOff(bool display, bool cursor, bool cursor_position);

#define SHIFT_DISPLAY 1
#define SHIFT_CURSOR 0
#define SHIFT_DIRECTION_RIGHT 1
#define SHIFT_DIRECTION_LEFT 0
void lcdEntryModeSet(bool shiftee_cursor0_or_display1, bool shift_direction_left0_right1);
void lcdCursorOrDisplayShift(bool shiftee_cursor0_or_display1, bool shift_direction_left0_right1);

#endif