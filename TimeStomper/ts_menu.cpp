#include <Arduino.h>
#include <stdint.h>
#include <math.h>
#include <stdlib.h>
#include <ts_menu.h>
#include <ts_lcd.h>
#include <ts_east.h>
#include <ts_calendar.h>

char getButtonPress(){
	char button = '\0';
	char temp = '\0';	
	while(temp = ts_query_keyboard_state()){		
		button = temp;
		delay(50);
	}	
	return button;
}

static bool isABC(char input){
	return input == 'A' || input == 'B' || input == 'C';
}

static bool isIdentityValid(uint16_t identity){
	if(identity == 0){
		lcdWriteLine(1,0, "0000 is reserved");
		lcdWriteLine(2,0, "Back to Home");
		delay(1500);
		return 0;
	}
	if(identity == 10000){
		lcdWriteLine(1, 0, "User Abort.");
		lcdWriteLine(2, 0, "Back to Home.");
		delay(1500);
		return 0;
	}	
	return 1;
}

static uint16_t askForUserId(char escape){		
	char input = 0;
	char input_list[5] = {' ', ' ', ' ', ' ', '\0'};
	uint8_t message_length = 10;
	uint8_t input_length = 4;	

	lcdWriteLine(1, 0, "Insert");
	lcdWriteLine(2, 0, "User ID: [");
	lcdWriteChar(2, message_length + input_length, ']');

	for(uint8_t i = 0; i < input_length; ++i)
		lcdWriteChar(2, message_length + i, input_list[i]);
	
	for(uint8_t j = 0; j < input_length; ++j){
		while(1){
			input = getButtonPress();

			if(!input)
				continue;

			if(input == escape)
				return 10000;

			if(isABC(input)){
				lcdWriteLine(1, 0, "Digits only!");
				delay(1500);
				lcdWriteLine(1, 0, "Insert");
				continue;
			}

			// if input is ok, proceed on the next input
			input_list[j] = input;
			for(uint8_t i = 0; i < input_length; ++i)
				lcdWriteChar(2, message_length + i, input_list[i]);

			break;
		}
		// add delay between accepted inputs, so the user has time to see it
		delay(250);
	}

	return strtoul(input_list, NULL, 10);	
}

void timeOfArrival(){
	lcdWriteLine(1, 0, "### Time of ### ");
	lcdWriteLine(2, 0, "### Arrival ### ");
	delay(1500);

	uint16_t identity = askForUserId('A');

	if(!isIdentityValid(identity))
		return;
	
	// update clock before stamping
	ts_internal_process();

	if( ts_create_time_stamp(identity, ts_get_wall_clock_time() ) ){
		lcdWriteLine(1, 0, "Success.");
		lcdWriteLine(2, 0, "Stamp saved.");
		delay(1500);
	}

	else {
		lcdWriteLine(1, 0, "Failure. ");
		lcdWriteLine(2, 0, "Out of memory.");
		delay(1500);
	}
}

void timeOfDeparture(){
	lcdWriteLine(1, 0, "### Time of ### ");
	lcdWriteLine(2, 0, "## Departure ## ");
	delay(1500);

	uint16_t identity = askForUserId('B');

	if(!isIdentityValid(identity))
		return;	

	// update clock before stamping
	ts_internal_process();

	if(ts_add_time_stamp_ending_time( identity, ts_get_wall_clock_time() ) ){
		lcdWriteLine(1, 0, "Success.");
		lcdWriteLine(2, 0, "Stamp saved.");
		delay(1500);
	}

	else {
		lcdWriteLine(1, 0, "Failure. No");
		lcdWriteLine(2, 0, "earlier stamp.");
		delay(1500);
	}
}

// TODO: view unended time stamps and their IDs
// TODO: reset time
void accumulatedTime(){
	lcdWriteLine(1, 0, "## Accumulated #");
	lcdWriteLine(2, 0, "#### Time #####");
	delay(1500);

	const uint16_t identity = askForUserId('C');

	if(!isIdentityValid(identity))
		return;

	char number_as_string[11] = ""; // 4 294 967 296 + null character
	size_t single_query = 1;
	uint32_t finished_stamp_count;	
	uint32_t total_time;
	ts_query_time_stamp_info(single_query, &identity, 0, ts_get_wall_clock_time(),
		&finished_stamp_count, &total_time);

	uint16_t days = total_time / 86400;
	uint16_t hours = total_time % 86400 / 3600;	
	uint8_t minutes = total_time % 3600 / 60;

	char cdays[5];
	char chours[3];
	char cminutes[3];

	sprintf(cdays, "%u", days);
	sprintf(chours, "%u", hours);
	sprintf(cminutes, "%u", minutes);

	char zero[2] = "0";
	char symbol_day[3] = "d ";
	char symbol_hour[3] = "h ";
	char symbol_minute[3] = "m ";
	char line_two[17];

	line_two[0] = '\0';

	if(days < 10){
		strcat(line_two, zero);
	}

	strcat(line_two, cdays);
	strcat(line_two, symbol_day);

	if(hours < 10){
		strcat(line_two, zero);
	}

	strcat(line_two, chours);
	strcat(line_two, symbol_hour);

	if(minutes < 10){
		strcat(line_two, zero);
	}

	strcat(line_two, cminutes);
	strcat(line_two, symbol_minute);

	char msg[2][8] = {
		"Time:",
		"Stamps:"
	};

	sprintf(number_as_string, "%u", finished_stamp_count);
	
	lcdWriteLine(1, 0, msg[0]);
	lcdWriteLine(2, 0, line_two);
	for(uint8_t i = 0, j = strlen(msg[1]); i < j; ++i){
		lcdWriteChar(1, 16 - strlen(msg[1]) + i, msg[1][i]);
		if(16 - strlen(number_as_string) + i <= 15){
			lcdWriteChar(2, 16 - strlen(number_as_string) + i, number_as_string[i]);
		}
	}

	while(!getButtonPress());	
}

void setTime(){	
	char inputs[6][5] = {
		"    ",
		"  ",
		"  ",
		"  ",
		"  ",
		"  "
	};

	char msg[6][10] = {
		"Year: [",
		"Month: [",
		"Day: [",
		"Hour: [",
		"Minute: [",
		"Second: ["
	};
		
	uint8_t input_length;
	uint8_t message_length;
	char input;
		
	for(uint8_t i = 0; i < 6; ++i){		
		input_length = strlen(inputs[i]);
		message_length = strlen(msg[i]);

		lcdWriteLine(1, 0, "Insert");
		lcdWriteLine(2, 0, msg[i]);

		for(uint8_t j = 0; j < input_length; ++j){
			lcdWriteChar(2, message_length + j, inputs[i][j]);			
		}
		
		lcdWriteChar(2, message_length + input_length, ']');

		for(uint8_t k = 0; k < input_length; ++k){
			while(1){
				input = getButtonPress();

				if(input == 'C'){
					lcdWriteLine(1, 0, "User Abort");
					lcdWriteLine(2, 0, "Back to Home");
					delay(1500);
					return;
				}

				if(input != 'A' && input != 'B' && input){
					break;
				}

			}
			inputs[i][k] = input;

			for(uint8_t j = 0; j < input_length; ++j){
				lcdWriteChar(2, message_length + j, inputs[i][j]);			
			}
						
			lcdWriteChar(2, message_length + input_length, ']');
		}

		delay(250);
	}	
	
	uint16_t year = strtoul(inputs[0], NULL, 10);	
	uint8_t month = strtoul(inputs[1], NULL, 10);	
	uint8_t day = strtoul(inputs[2], NULL, 10);	
	uint8_t hour = strtoul(inputs[3], NULL, 10);	
	uint8_t minute = strtoul(inputs[4], NULL, 10);	
	uint8_t second = strtoul(inputs[5], NULL, 10);	
	
	Date date = makeDate(year, month, day, hour, minute, second);
	uint32_t seconds_since_epoch = convertDateToSeconds(date);	

	// Without calling this, the set time would be off by the amount it took to give the inputs.
	// During handling of user inputs the clock is not being updated, unless explicitly updated,
	// by calling this function.
	ts_internal_process();

	ts_save_wall_clock_time(seconds_since_epoch);
	ts_set_wall_clock_time(seconds_since_epoch);
	lcdWriteLine(1, 0, "Time is set.");
	lcdWriteLine(2, 0, "Back to Home");
	delay(1500);
}

void dateMenu(){
	lcdWriteLine(1, 0, "##### Date #####");
	lcdWriteLine(2, 0, "##### Menu #####");
	delay(1500);

	char input = '\0';
	while(input == '\0'){
		lcdWriteLine(1, 0, "Set Time?");
		lcdWriteLine(2, 0, "1=yes 2=no");
		input = getButtonPress();
	}

	if(input == '1'){
		setTime();
	}
}

void memoryMenu(){
	lcdWriteLine(1, 0, "#### Memory ####");
	lcdWriteLine(2, 0, "##### Menu #####");
	delay(1500);

	char input = '\0';
	while(input == '\0'){
		lcdWriteLine(1, 0, "Erase all data?");
		lcdWriteLine(2, 0, "1=yes 2=no");
		input = getButtonPress();
	}

	if(input == '1'){
		lcdWriteLine(1, 0, "Erasing memory");
		lcdWriteLine(2, 0, "Please Wait");
		ts_erase_time_information();
		lcdWriteLine(1, 0, "Memory erased");
		lcdWriteLine(2, 0, "Back to Home");
		delay(1500);
	}

	else
	{
		lcdWriteLine(1, 0, "User Abort");
		lcdWriteLine(2, 0, "Back to Home");
		delay(1500);
	}
}

void adminConfig(){
	lcdWriteLine(1, 0, "### Admin ###");
	lcdWriteLine(2, 0, "### Config ###");
	delay(1500);	

	char input = '\0';
	while(input == '\0'){
		lcdWriteLine(1, 0, "1=date 2=memory");
		lcdWriteLine(2, 0, "3=exit");
		input = getButtonPress();
	}

	switch(input){
		case '1': dateMenu(); break;
		case '2': memoryMenu(); break;
	}
}
