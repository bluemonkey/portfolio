#include "Master.h"
#include <iostream>

Master::Master()
{
    count_ = std::thread::hardware_concurrency() - 1;    
    workers_.reserve(count_);
    for (unsigned short i = 0; i < count_; ++i)
        workers_.emplace_back();
}

void Master::begin()
{
    for (unsigned i = 0; i < count_; ++i)
        workers_.at(i).running_.store(true, std::memory_order_release);
}

void Master::end()
{
    for (unsigned i = 0; i < count_; ++i)
        workers_.at(i).running_.store(false, std::memory_order_release);

    waitMany();
}

void Master::push(FuncPtr func, VoidPtr data, unsigned short workerIndex)
{
    // do it yourself    
    if (workerIndex == count_)
        func(data); 

    else
    {        
        while (isBusyWorker(workerIndex));        
        workers_.at(workerIndex).atomicTask_.store(new Task(func, data), std::memory_order_release);
        workers_.at(workerIndex).ready_.store(true, std::memory_order_release);
    } 
}

void Master::waitSingle(unsigned short workerIndex)
{
    if (workerIndex < count_)
        while (isBusyWorker(workerIndex));
}

void Master::waitMany()
{    
    while (areBusyWorkers());
}

bool Master::isBusyWorker(unsigned short workerIndex)
{
    return workers_.at(workerIndex).busy_.load(std::memory_order_acquire);
}

bool Master::areBusyWorkers()
{
    for (unsigned short i = 0; i < count_; ++i)
    {
        if (!isBusyWorker(i)) continue;
        else return true;
    }
    return false;
}

