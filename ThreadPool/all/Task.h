#ifndef CUSTOM_TASK_INCLUDE
#define CUSTOM_TASK_INCLUDE
#include "FuncPtr.h"
#include "VoidPtr.h"

struct Task
{
public:    
    Task(FuncPtr func, VoidPtr data);
    FuncPtr func;
    VoidPtr data;
    void execute();
};
#endif