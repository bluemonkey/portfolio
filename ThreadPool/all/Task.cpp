#include "Task.h"
#include <iostream>

Task::Task(FuncPtr func_, VoidPtr data_) : func(func_), data(data_){}
void Task::execute()
{
    func(data);
}