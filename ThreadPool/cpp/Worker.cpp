#include "Worker.h"
Worker::Worker()
{    
    atomicTask_.store(nullptr, std::memory_order_release);
    busy_.store(false, std::memory_order_release);
    ready_.store(false, std::memory_order_release);
    thread_ = std::move(std::thread(&Worker::work, this));    
}

Worker::Worker(Worker const&){}

Worker::~Worker()
{
    if (thread_.joinable())
        thread_.join();
}

void Worker::work()
{    
    while(running_.load(std::memory_order_acquire))
    {        
        if(ready_.load(std::memory_order_acquire))
        {
            busy_.store(true, std::memory_order_release);
            Task* task = atomicTask_.load(std::memory_order_acquire);
            task->execute();
            delete task;        
            ready_.store(false, std::memory_order_release);
            busy_.store(false, std::memory_order_release);            
        }
    } 
}
