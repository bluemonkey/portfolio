#include <iostream>
#include "VoidPtr.h"
#include "Master.h"
#include <chrono>

unsigned expected = 1000000000;
unsigned batchSize = expected /4;

void mockFunc(VoidPtr data)
{            
    unsigned result = (unsigned)data;    
    //std::cout << "count: " << result << "\n";    
    for (unsigned j = batchSize + result; result < j; ++result);
    data = (VoidPtr)result;
}

// this function works as if it were a synchronization queue, but there are no
// data structures in use, just sequencial lines of code :)
// the function begins with master.begin() and ends with master.end() to
// ensure side threads will be waited
// master.begin() spawns the threads
// master.end() despawns the threads
// to ensure the overall performance of the program goes smooth it's best to
// spawn the threads only once and despawn upon program termination 
void test(unsigned& count0, unsigned& count1, unsigned& count2, unsigned& count3)
{    
    Master master;
    master.begin();
    /*
    std::cout << "[INITIAL VALUES]\n";
    std::cout << "count0: " << count0 << "\n";
    std::cout << "count1: " << count1 << "\n";
    std::cout << "count2: " << count2 << "\n";
    std::cout << "count3: " << count3 << "\n";    
    std::cout << "\n\n";

    std::cout << "[PUSH - 1ST BATCH]:\n"; */
    master.push(mockFunc, (VoidPtr)count0, 0);    
    master.push(mockFunc, (VoidPtr)count1, 1);    
    master.push(mockFunc, (VoidPtr)count2, 2);    
    master.push(mockFunc, (VoidPtr)count3, 3); /*
    std::cout << "\n\n";
    std::cout << "master.deleteMeASAP: " << master.deleteMeASAP << "\n";
    std::cout << "\n\n";
    */

    /*
    std::cout << "[QUICK RESULT - 1ST BATCH]\n";
    std::cout << "count0: " << count0 << "\n";
    std::cout << "count1: " << count1 << "\n";
    std::cout << "count2: " << count2 << "\n";
    std::cout << "count3: " << count3 << "\n";     
    std::cout << "\n\n";

    std::cout << "[WAIT - 1ST BATCH]\n"; */
    //master.waitMany();
    /*
    std::cout << "first batch complete!\n";
    std::cout << "\n\n";

    std::cout << "[FINAL RESULT - 1ST BATCH] \n";
    std::cout << "count0: " << count0 << "\n";
    std::cout << "count1: " << count1 << "\n";
    std::cout << "count2: " << count2 << "\n";
    std::cout << "count3: " << count3 << "\n";    
    std::cout << "\n\n";
    
    std::cout << "[PUSH - 2ND BATCH] \n"; */
    master.push(mockFunc, (VoidPtr)count0, 0);
    master.push(mockFunc, (VoidPtr)count1, 1);
    master.push(mockFunc, (VoidPtr)count2, 2);
    master.push(mockFunc, (VoidPtr)count3, 3); /*
    std::cout << "\n\n";

    
    std::cout << "master.deleteMeASAP: " << master.deleteMeASAP << "\n";
    std::cout << "\n\n";
    

    std::cout << "[QUICK RESULT - 2ND BATCH]\n";
    std::cout << "count0: " << count0 << "\n";
    std::cout << "count1: " << count1 << "\n";
    std::cout << "count2: " << count2 << "\n";
    std::cout << "count3: " << count3 << "\n";    
    std::cout << "\n\n";

    std::cout << "[WAIT - 2ND BATCH]\n"; */
    //master.waitMany();
/*
    std::cout << "second batch complete!\n";
    std::cout << "\n\n";

    std::cout << "[FINAL RESULT - 2ND BATCH]\n";
    std::cout << "count0: " << count0 << "\n";
    std::cout << "count1: " << count1 << "\n";
    std::cout << "count2: " << count2 << "\n";
    std::cout << "count3: " << count3 << "\n";    
    std::cout << "\n\n"; */
    // master.waitSingle(3); no need to wait the main execution context :)
    master.end(); // end always waits for workers to finish their tasks
}

int main()
{
    // data in use
    unsigned count0 = 0;
    unsigned count1 = 0;
    unsigned count2 = 0;
    unsigned count3 = 0;

    // time quad core performance
    auto start = std::chrono::high_resolution_clock::now();
    test(count0, count1, count2, count3);
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> elapsed = end - start;
    double test = elapsed.count();
    
    // print results
    std::cout << "[FINAL RESULT - ALL BATCHES]\n";
    std::cout << "count0: " << count0 << "\n";
    std::cout << "count1: " << count1 << "\n";
    std::cout << "count2: " << count2 << "\n";
    std::cout << "count3: " << count3 << "\n";
    std::cout << "multicore count: " << count0 + count1 + count2 + count3 << std::endl;
    std::cout << "time elapsed: " << test << "ms" << std::endl;
    std::cout << std::endl;

    // time single core performance
    start = std::chrono::high_resolution_clock::now();
    unsigned  i = 0;
    for (unsigned  j = expected*2; i < j; ++i){}
    end = std::chrono::high_resolution_clock::now();
    elapsed = end - start;
    double singleCore = elapsed.count();

    // print results
    std::cout << "singlecore count: " << i << std::endl;
    std::cout << "time elapsed: " << singleCore << "ms" << std::endl;
    std::cout << std::endl;

    // print differences in results
    std::cout << "factor: " << singleCore / test << std::endl;
    std::cout << "if factor < 1, then single core solution was faster" << std::endl;
    std::cout << "if factor > 1, then multicore core solution was faster" << std::endl;
    std::cout << "max factor is core count, but impossible to reach\n" << std::endl;
    std::cout << "4-core(?) efficiency: " << singleCore / test / 4 * 100 << "%" << std::endl;    
    
    system("pause");
    return 0;
}