#ifndef CUSTOM_MASTER_INCLUDE
#define CUSTOM_MASTER_INCLUDE
#include "FuncPtr.h"
#include "VoidPtr.h"
#include "Worker.h"
#include <vector>

class Master
{
public:
    Master();
    void begin();
    void end();
    void push(FuncPtr func, VoidPtr data, unsigned short workerIndex);
    void waitMany();
    void waitSingle(unsigned short workerIndex);    
private:
    std::vector<Worker> workers_;
    unsigned count_;    
    bool isBusyWorker(unsigned short workerIndex);
    bool areBusyWorkers();
};
#endif