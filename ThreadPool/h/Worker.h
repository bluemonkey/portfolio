#ifndef CUSTOM_WORKER_INCLUDE
#define CUSTOM_WORKER_INCLUDE
#include <thread>
#include <atomic>
#include <memory>
#include "Task.h"

class Worker
{
public:
    Worker();
    Worker(Worker const&);
    ~Worker();

private:
    friend class Master;
    std::thread thread_;
    std::atomic<Task*> atomicTask_;
    std::atomic<bool> busy_;
    std::atomic<bool> ready_;
    std::atomic<bool> running_;
    void work();
};
#endif