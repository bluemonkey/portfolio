#ifndef CUSTOM_FUNCPTR_INCLUDE
#define CUSTOM_FUNCPTR_INCLUDE
#include "VoidPtr.h"
using FuncPtr = void(*)(VoidPtr);
#endif