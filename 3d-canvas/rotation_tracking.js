function random_rotation(){
  var choice = parseInt(Math.random()*3);
  var direction = parseInt(Math.random()*2);
  var obj = {};
  switch(choice){
    case 0: obj.rotation = 'x'; break;
    case 1: obj.rotation = 'y'; break;
    case 2: obj.rotation = 'z'; break;
  }
  switch(direction){
  	case 0: obj.direction =  1; break;
  	case 1: obj.direction = -1; break;
  }
  return obj;
}

var rotations = [];

function next(){
	var obj = random_rotation();
	var last = rotations[rotations.length-1];

	if(last === undefined){
		rotations.push({rotation: obj.rotation, count: obj.direction});
	}
	else if(last !== undefined && last.rotation !== undefined){
		if(last.rotation === obj.rotation){
			rotations[rotations.length-1].count += obj.direction;
		}
		else if(last.rotation !== obj.rotation){
			rotations.push({rotation: obj.rotation, count: obj.direction});
		}
	}

	var debug_text = "";
	for(var i = 0, j = rotations.length; i < j; ++i){
		debug_text += rotations[i].rotation + " " + rotations[i].count + "\n";
	}
	console.log("%s", debug_text);
}