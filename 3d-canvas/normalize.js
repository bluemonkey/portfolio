function normalize(value, min, max){
	var a = value - min;
	var b = max - value;
	return a / (a+b);
}

function normalize_2d(point_2d){
	var magnitude = Math.sqrt(point_2d.x**2 + point_2d.y**2);
	point_2d.x /= magnitude;
	point_2d.y /= magnitude;
	return point_2d;
}

function normalize_3d(point_3d){
	var magnitude = Math.sqrt(point_3d.x**3 + point_3d.y**2 + point_3d.z**2);
	point_3d.x /= magnitude;
	point_3d.y /= magnitude;
	point_3d.z /= magnitude;
	return point_3d;
}