function Spherical_Coordinates(vector3){
	this.theta = Math.acos(vector3.z);					// range: -1 to 1 -> 0 to π
	this.phi = Math.atan2(vector3.y, vector.x); // range: -π to π
}

function debug_print_some_arccosines(){
	for(var i = -1; i <= 1; i = parseFloat((i +0.01).toFixed(2))){
		console.log("[%f]: %f", i, Math.acos(i));
	}
}

function debug_print_some_arctangent2s(){
	var normalized_y = [];
	var normalized_x = [];
	for(var i = -1; i <= 1; i = parseFloat((i +0.01).toFixed(2))){
		normalized_y.push(i);
		normalized_x.push(i);
	}

	for(var i = 0, j = normalized_y.length; i < j; ++i){
		for(var k = 0, l = normalized_x.length; k < l; ++k){
			console.log("[%f, %f]: %f", normalized_y[i], normalized_x[k], Math.atan2(normalized_y[i], normalized_x[k]));
		}
	}
}

/* some notes about atan2:
	if( (y >= +0) && x(-1:1) ), then (result >= +0 && result <= +π)
	if( (y <= -0) && x(-1:1) ), then (result >= -π && result <= -0)
*/