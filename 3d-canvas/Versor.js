function Versor(vector3){
	var magnitude_reciprocal = 1 / Math.sqrt(vector3.x**2 + vector3.y**2 + vector3.z**2);
	this.x = vector3.x *= magnitude_reciprocal;
	this.y = vector3.y *= magnitude_reciprocal;
	this.z = vector3.z *= magnitude_reciprocal;
}