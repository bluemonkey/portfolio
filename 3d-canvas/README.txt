TO TRANSLATE THE CAMERA IN THE VIEWPORT: Q,W,E,A,S,D
TO TRANSLATE THE CUBE IN THE VIEWPORT: Z,X,C,V,B,N
TO ROTATE THE CUBE ALONG THE GLOBAL COORDINATE AXIS: 1,2,3,4,5,6
LOCAL COORDINATE ROTATIONS: not implemented

Note:
if you are planning to rotate the cube back to its original orientation, you must do the same rotations backward and opposite direction

press F5 to refresh