function find_blur_proof_max_dimensions(pixel_budget_horizontal, pixel_budget_vertical, number){
	var fractions = inefficient_and_possibly_hanging_find_fraction(number);
	var interval = fractions.numerator * fractions.denominator;

	var remainder_horizontal = pixel_budget_horizontal % interval;
	var remainder_vertical = pixel_budget_vertical % interval;

	var width = pixel_budget_horizontal - remainder_horizontal;
	var height = pixel_budget_vertical - remainder_vertical;

	return {
		width: width,
		height: height,
		remainder_horizontal: remainder_horizontal,
		remainder_vertical: remainder_vertical,
		interval: interval
	};
}

function inefficient_and_possibly_hanging_find_fraction(number){
	var i = 1;
	while(true){
		if(++i * number % 1 === 0){
			return {
				numerator: i * number,
				denominator: i
			};

		}
		if(i === 1000000){
			var notify = document.createElement("div");
			notify.innerText = "This zoom level is not supported.";
			document.body.appendChild(notify);
			break;
		}
	}
}

function set_blur_proof_canvas_dimensions(canvas){
	var horizontal_scrollbar_artifact_width = 3;
	var nice_dimensions = find_blur_proof_max_dimensions(window.innerWidth, window.innerHeight, window.devicePixelRatio);
	canvas.width = nice_dimensions.width * window.devicePixelRatio;
	canvas.height = nice_dimensions.height * window.devicePixelRatio;
	canvas.style.width = nice_dimensions.width + "px";
	canvas.style.height = nice_dimensions.height + "px";
	canvas.style.marginTop = nice_dimensions.remainder_vertical / 2 + "px";
	canvas.style.marginLeft = nice_dimensions.remainder_horizontal / 2 - Math.floor(horizontal_scrollbar_artifact_width/2) + "px";
}

function disable_scrollbars(){
	// hide vertical and horizontal bars for the whole page (<html> element)
	document.documentElement.style.overflow = "hidden"; 
}


function Point2D(x, y){
	this.x = x;
	this.y = y;
}

function Point3D(x, y, z){
	this.x = x;
	this.y = y;
	this.z = z;
}

function Orientation(x, y, z){
	this.x = undefined;
	this.y = undefined;
	this.z = undefined;
	this.deg = {};
	this.deg.x = x;
	this.deg.y = y;
	this.deg.z = z;
	this.set = function(){
		this.x = degrees_to_radians(this.deg.x);
		this.y = degrees_to_radians(this.deg.y);
		this.z = degrees_to_radians(this.deg.z);
	}
	this.set();
}

function degrees_to_radians(degrees){
	return degrees / 180 * Math.PI;
}

function radians_to_degrees(radians){
	return radians / Math.PI * 180;
}

function draw_cube(points_2d){
  g.context.strokeStyle = "black";
	g.context.beginPath();
	g.context.moveTo(points_2d[0].x, points_2d[0].y);
	g.context.lineTo(points_2d[1].x, points_2d[1].y);
	g.context.lineTo(points_2d[2].x, points_2d[2].y);
	g.context.lineTo(points_2d[3].x, points_2d[3].y);
	g.context.lineTo(points_2d[0].x, points_2d[0].y);
	g.context.stroke();

	g.context.beginPath();
	g.context.moveTo(points_2d[4].x, points_2d[4].y);
	g.context.lineTo(points_2d[5].x, points_2d[5].y);
	g.context.lineTo(points_2d[6].x, points_2d[6].y);
	g.context.lineTo(points_2d[7].x, points_2d[7].y);
	g.context.lineTo(points_2d[4].x, points_2d[4].y);
	g.context.stroke();

	g.context.beginPath();
	g.context.moveTo(points_2d[0].x, points_2d[0].y);
	g.context.lineTo(points_2d[4].x, points_2d[4].y);
	g.context.stroke();

	g.context.beginPath();
	g.context.moveTo(points_2d[1].x, points_2d[1].y);
	g.context.lineTo(points_2d[5].x, points_2d[5].y);
	g.context.stroke();

	g.context.beginPath();
	g.context.moveTo(points_2d[2].x, points_2d[2].y);
	g.context.lineTo(points_2d[6].x, points_2d[6].y);
	g.context.stroke();

	g.context.beginPath();
	g.context.moveTo(points_2d[3].x, points_2d[3].y);
	g.context.lineTo(points_2d[7].x, points_2d[7].y);
	g.context.stroke();

	//debug_points(points_2d, cube);
}

function draw_labels(points_2d){
	g.context.font = "20px OpenSansLight";
	var A = Math.ceil(g.context.measureText("A").width);
	var B = Math.ceil(g.context.measureText("B").width);
	var C = Math.ceil(g.context.measureText("C").width);
	var D = Math.ceil(g.context.measureText("D").width);
	var E = Math.ceil(g.context.measureText("E").width);
	var F = Math.ceil(g.context.measureText("F").width);
	var G = Math.ceil(g.context.measureText("G").width);
	var H = Math.ceil(g.context.measureText("H").width);
	g.context.fillText("A", points_2d[0].x - A, points_2d[0].y);
	g.context.fillText("B", points_2d[1].x - B, points_2d[1].y);
	g.context.fillText("C", points_2d[2].x - C, points_2d[2].y);
	g.context.fillText("D", points_2d[3].x - D, points_2d[3].y);
	g.context.fillText("E", points_2d[4].x - E, points_2d[4].y);
	g.context.fillText("F", points_2d[5].x - F, points_2d[5].y);
	g.context.fillText("G", points_2d[6].x - G, points_2d[6].y);
	g.context.fillText("H", points_2d[7].x - H, points_2d[7].y);
}

function draw_world_axis(points_2d){
	g.context.strokeStyle = "red";
	g.context.beginPath();
	g.context.moveTo(points_2d[0].x, points_2d[0].y); // neg x
	g.context.lineTo(points_2d[1].x, points_2d[1].y); // pos x
	g.context.stroke();

	g.context.strokeStyle = "green";
	g.context.beginPath();
	g.context.moveTo(points_2d[2].x, points_2d[2].y); // neg y
	g.context.lineTo(points_2d[3].x, points_2d[3].y); // pos y
	g.context.stroke();

	g.context.strokeStyle = "blue";
	g.context.beginPath();
	g.context.moveTo(points_2d[4].x, points_2d[4].y); // neg z
	g.context.lineTo(points_2d[5].x, points_2d[5].y); // pos z
	g.context.stroke();
}

function rotate(scene_object, degx, degy, degz){
	var tx = degrees_to_radians(degx);
	var ty = degrees_to_radians(degy);
	var tz = degrees_to_radians(degz);

	var x, y, z;

	var vertex_count = scene_object.vertices.length;

	for(var i = 0; i < vertex_count; ++i){
		y = scene_object.vertices[i].y;
		z = scene_object.vertices[i].z;
		scene_object.vertices[i].y = y * Math.cos(tx) + z * -Math.sin(tx);
		scene_object.vertices[i].z = y * Math.sin(tx) + z * Math.cos(tx);
	}

	for(var i = 0; i < vertex_count; ++i){
		x = scene_object.vertices[i].x;
		z = scene_object.vertices[i].z;
		scene_object.vertices[i].x = x * Math.cos(ty) + z * Math.sin(ty);
		scene_object.vertices[i].z = x * -Math.sin(ty) + z * Math.cos(ty);
	}

	for(var i = 0; i < vertex_count; ++i){
		x = scene_object.vertices[i].x;
		y = scene_object.vertices[i].y;
		scene_object.vertices[i].x = x * Math.cos(tz) + y * -Math.sin(tz);
		scene_object.vertices[i].y = x * Math.sin(tz) + y * Math.cos(tz);
	}
}

function translate(scene_object, x, y, z){
	var vertex_count = scene_object.vertices.length;
	for(var i = 0; i < vertex_count; ++i){
		scene_object.vertices[i].x += x;
		scene_object.vertices[i].y += y;
		scene_object.vertices[i].z += z;
	}
}

function tabula_rasa(){
	g.context.putImageData(g.blank_slate, 0, 0);
}

function project_2D(point3d, camera){
	var ax = point3d.x - camera.vertices[0].x; // camera pinhole x
	var ay = point3d.y - camera.vertices[0].y; // camera pinhole y
	var bz = camera.get_image_plane_distance(); // -1
	var az = point3d.z - camera.vertices[0].z; // camera pinhole z

	var bx = ax*(bz/az); // image plane x coordinate of the point in question, unnormalized
	var by = ay*(bz/az); // image plane y coordinate of the point in question, unnormalized
	console.log("bx: %f, by: %f", bx, by);

	// all cube coordinates fall in the range of -1 to 1, so this only works with the current kind of cubes
	// todo: object should know its min max ranges, so that it can be provided to the normalizer
	bx = normalize_a_point_in_range(bx, -1, 1); // image plane x coordinate of the point in questio, normalized
	by = normalize_a_point_in_range(by, -1, 1); // image plane y coordinate of the point in questio, normalized
	console.log("nbx: %f, nby: %f", bx, by);

	// normalized coordinates are mapped to the canvas dimensions
	var horizontal_offset = (canvas.width - canvas.height) / 2;
	bx = bx * canvas.height + horizontal_offset;
	by = by * canvas.height;
	return new Point2D(bx, by);
}

function get_2d_points(object_3d, camera){
	var points_2d = [];
	for(var i = 0, j = object_3d.vertices.length; i < j; ++i)
		points_2d[i] = project_2D(object_3d.vertices[i], camera);
	
	return points_2d;
}

// CUBE
// cube has eight 3D to define it
function Cube(x, y, z){
	this.vertices = [];
	this.vertices[0] = new Point3D(x - 1, y + 1, z + 1), // A - front - bottom - left
	this.vertices[1] = new Point3D(x + 1, y + 1, z + 1), // B - front - bottom - right
	this.vertices[2] = new Point3D(x + 1, y - 1, z + 1), // C - front - top    - right
	this.vertices[3] = new Point3D(x - 1, y - 1, z + 1), // D - front - top    - left
	this.vertices[4] = new Point3D(x - 1, y + 1, z - 1), // E - back  - bottom - left
	this.vertices[5] = new Point3D(x + 1, y + 1, z - 1), // F - back  - bottom - right
	this.vertices[6] = new Point3D(x + 1, y - 1, z - 1), // G - back  - top    - right
	this.vertices[7] = new Point3D(x - 1, y - 1, z - 1)  // H - back  - top    - left
	this.orientation = new Point3D(0, 0, 0);
}

function Camera(x, y, z){
	this.vertices = [];
	this.vertices[0] 							= new Point3D(x, y, z); 						 // pinhole, the only front vertex
	this.vertices[1]     					= new Point3D(x - 1, y - 1, z - 1); // display surface aka image plane back-top-left
	this.vertices[2]    					= new Point3D(x + 1, y - 1, z - 1); // display surface aka image plane back-top-right
	this.vertices[3] 							= new Point3D(x + 1, y + 1, z - 1); // display surface aka image plane back-bottom-right
	this.vertices[4]  						= new Point3D(x - 1, y + 1, z - 1); // display surface aka image plane back-bottom-left
	this.orientation              = new Point3D(0, 0, 0);
	this.get_image_plane_distance = function(){
		return -1;
	}
}

function perspective_divide(point_3d){
	var bx = point_3d.x / -point_3d.z;
	var by = point_3d.y / -point_3d.z;
	return new Point2D(bx, by);
}

function normalize_a_point_in_range(value, min, max){
	var a = value - min;
	var b = max - value;
	return a / (a+b);
}

function normalize_2d(point_2d){
	var magnitude = Math.sqrt(point_2d.x**2 + point_2d.y**2);
	point_2d.x = Math.abs(point_2d.x) / magnitude;
	point_2d.y = Math.abs(point_2d.y) / magnitude;
	
	return point_2d;
}

function remap_to_canvas(point_2d, width, height){
	point_2d.x *= width;
	point_2d.y *= height;
	return point_2d;
}

function Axis(w, h){
	var hypo = Math.sqrt( Math.pow(w, 2) + Math.pow(h, 2));
	this.vertices = [];
	this.vertices[0] = new Point3D(0, 0, 0); 
	this.vertices[1] = new Point3D(hypo, 0, 0); 
	this.vertices[2] = new Point3D(0, 0, 0); 
	this.vertices[3] = new Point3D(0, hypo, 0); 
	this.vertices[4] = new Point3D(0, 0, 0); 
	this.vertices[5] = new Point3D(0, 0, hypo);
}

function center_of_points(object_3d){
	var bx, by, bz; // bigger values
	var sx, sy, sz; // smaller values
	bx = by = bz = -Infinity;
	sx = sy = sz = Infinity;
	
	for(var i = 0, j = object_3d.vertices.length; i < j; ++i){
		if(object_3d.vertices[i].x >= bx)
			bx = object_3d.vertices[i].x;

		if(object_3d.vertices[i].x < sx)
			sx = object_3d.vertices[i].x;

		if(object_3d.vertices[i].y >= by)
			by = object_3d.vertices[i].y;

		if(object_3d.vertices[i].y < sy)
			sy = object_3d.vertices[i].y;

		if(object_3d.vertices[i].z >= bz)
			bz = object_3d.vertices[i].z;

		if(object_3d.vertices[i].z < sz)
			sz = object_3d.vertices[i].z;
	}

	return {
		x: (bx-sx)/2+sx, 
		y: (by-sy)/2+sy, 
		z: (bz-sz)/2+sz
	};
}


// globals
var g;

// MAIN
function main(canvas){
	disable_scrollbars();
	set_blur_proof_canvas_dimensions(canvas);

	var context     = canvas.getContext("2d");
	var blank_slate = context.getImageData(0, 0, canvas.width, canvas.height);

	window.addEventListener("keydown", on_key_down);

	var render_targets = {
		"center-cube": new Cube(0, 0, 0),
	};

	var camera          = new Camera(0, 0, 0);

	g                 = {};
	g.context         = context;
	g.blank_slate     = blank_slate;
	g.render_targets  = render_targets;
	g.camera          = camera;
	render_scene();
}

function render_scene(){
	tabula_rasa();
	var cube_center;
	var points_2d;
	
	console.log("render targets:");
	for(var id in g.render_targets){
		console.log("%O", g.render_targets[id]);
		if(id === "center-cube"){
			 cube_center = center_of_points(g.render_targets[id]);
		 }
		points_2d = get_2d_points(g.render_targets[id], g.camera);
		switch(id){
			case "axis": 				draw_world_axis(points_2d);                   break;
			case "center-cube": draw_cube(points_2d), draw_labels(points_2d); break;
			case "cube": 				draw_cube(points_2d);				                  break;
		}
	}

	var center_cube_orientation = g.render_targets["center-cube"].orientation;

	g.context.font                   = "20px OpenSansLight";
  var camera_text                  = "Camera: {x: "            +g.camera.vertices[0].x.toString()    +", y: "+g.camera.vertices[0].y.toString()    +", z: "+g.camera.vertices[0].z.toString()    +"}";
	var cube_center_text             = "Cube Center: {x: "       +cube_center.x.toString()             +", y: "+cube_center.y.toString()             +", z: "+cube_center.z.toString()             +"}";
	var cube_center_orientation_text = "Cube Orientation: {x: "  +center_cube_orientation.x.toString() +", y: "+center_cube_orientation.y.toString() +", z: "+center_cube_orientation.z.toString() +"}";

	var camera_text_length          = Math.ceil(g.context.measureText(camera_text).width);
	var cube_center_text_length     = Math.ceil(g.context.measureText(cube_center_text).width);
	var cube_center_orientation_text_length = Math.ceil(g.context.measureText(cube_center_orientation_text).width);
	
	g.context.fillText(camera_text, 				 canvas.width - camera_text_length,          parseInt(g.context.font)*1);
	g.context.fillText(cube_center_text, 		 canvas.width - cube_center_text_length,     parseInt(g.context.font)*2);
	g.context.fillText(cube_center_orientation_text, 		 canvas.width - cube_center_orientation_text_length,     parseInt(g.context.font)*3);
}

function on_key_down(e){
	switch(e.key){
		case 'q': translate(g.camera,  1,  0,  0); render_scene(); break;
		case 'w': translate(g.camera,  0,  1,  0); render_scene(); break;
		case 'e': translate(g.camera,  0,  0,  1); render_scene(); break;
		case 'a': translate(g.camera, -1,  0,  0); render_scene(); break;
		case 's': translate(g.camera,  0, -1,  0); render_scene(); break;
		case 'd': translate(g.camera,  0,  0, -1); render_scene(); break;
		case 'z': translate(g.render_targets["center-cube"],  1,  0,  0); render_scene(); break;
		case 'x': translate(g.render_targets["center-cube"],  0,  1,  0); render_scene(); break;
		case 'c': translate(g.render_targets["center-cube"],  0,  0,  1); render_scene(); break; 
		case 'v': translate(g.render_targets["center-cube"], -1,  0,  0); render_scene(); break;
		case 'b': translate(g.render_targets["center-cube"],  0, -1,  0); render_scene(); break;
		case 'n': translate(g.render_targets["center-cube"],  0,  0, -1); render_scene(); break;
		case '1': g.render_targets["center-cube"].orientation.x += 1; rotate(g.render_targets['center-cube'],  1,  0,  0); render_scene(); break;
		case '2': g.render_targets["center-cube"].orientation.y += 1; rotate(g.render_targets['center-cube'],  0,  1,  0); render_scene(); break;
		case '3': g.render_targets["center-cube"].orientation.z += 1; rotate(g.render_targets['center-cube'],  0,  0,  1); render_scene(); break;
		case '4': g.render_targets["center-cube"].orientation.x -= 1; rotate(g.render_targets['center-cube'], -1,  0,  0); render_scene(); break;
		case '5': g.render_targets["center-cube"].orientation.y -= 1; rotate(g.render_targets['center-cube'],  0, -1,  0); render_scene(); break;
		case '6': g.render_targets["center-cube"].orientation.z -= 1; rotate(g.render_targets['center-cube'],  0,  0, -1); render_scene(); break;
	}
}

/*
// MAIN
function main2(canvas){
	disable_scrollbars();
	set_blur_proof_canvas_dimensions(canvas);

	context = canvas.getContext("2d");
	blank_slate = context.getImageData(0, 0, canvas.width, canvas.height);

	register_non_blur_stroking_method(context);
	window.addEventListener("keydown", on_key_down);

	cube = new Cube(5, -5, -10);
	rotate(cube, 45, 45, 100);
	

	camera = new Point3D(0, 0, 0); 
	display_surface = new Point3D(0, 0, -1);

	var points_2d = [];
	for(var i = 0, j = 8; i < j; ++i){
		var point_2d = perspective_divide(cube.vertices[i]);
		point_2d.x = normalize_a_point_in_range(point_2d.x, -1, 1);
		point_2d.y = normalize_a_point_in_range(point_2d.y, -1, 1);
		point_2d = remap_to_canvas(point_2d, canvas.height, canvas.height);
		points_2d[i] = point_2d;
	}
	

	draw_cube(points_2d);
	draw_labels(points_2d);
	

	var world_axis_hypotenuse = Math.sqrt( Math.pow(canvas.width, 2) + Math.pow(canvas.height, 2));
	world_axis = [
		new Point3D(0, 0, 0),
		new Point3D(world_axis_hypotenuse, 0, 0),
		new Point3D(0, 0, 0),
		new Point3D(0, world_axis_hypotenuse, 0),
		new Point3D(0, 0, 0),
		new Point3D(0, 0, world_axis_hypotenuse)
	];

	camera = new Point3D(0, 0, 0); 
	orientation = new Orientation(0, 0, 0);
	display_surface = new Point3D(0, 0, 1);
	orientation.set();
	render_scene();
}

*/