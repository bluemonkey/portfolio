// global access point
global.app = {};

// app specific modules
global.app.main = require("./app2/main2");
global.app.request = require("./app2/request2");

// program start
global.app.main();