module.exports = saveRequestResult;

function saveRequestResult(){
  switch(global.app.requestQuery){
    case "balances"  : global.app.balances   = global.app.requestResult; break;
    case "openOrders": global.app.openOrders = global.app.requestResult; break;
    case "markets"   : global.app.markets    = global.app.requestResult; break;
    case "summaries" : global.app.summaries  = global.app.requestResult; break;
    case "orderbook" : global.app.orderbook  = global.app.requestResult; break;
  }
  global.app.requestResult = "";
  global.app.main();
}