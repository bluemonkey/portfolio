module.exports = request;

global.app.requestQuery = undefined;
global.app.requestOrder = undefined;

function request(query, order){
  global.app.requestQuery = query;
  global.app.requestOrder = order;
  if(order !== undefined){
    if(typeof order === "object"){
      console.log("market: '"+order.market+"', quantity: '"+order.quantity+"', rate: '"+order.rate+"'");
    }
    else {
      console.log("uuid: '"+order+"'");
    }
  }
  switch(query){
    // experimental
    case "balances": accountGetBalances(); break;
    case "openOrders": marketGetOpenOrders(); break;
    case "markets": publicGetMarkets(); break;
    case "summaries": publicGetMarketSummaries(); break;
    case "orderbook": publicGetOrderbook(order); break;

    // core functions
    case "buy": marketBuyLimit(order.market, order.quantity, order.rate); break;
    case "sell": marketSellLimit(order.market, order.quantity, order.rate); break;
    case "cancel-buy": marketCancel(order); break;
    case "cancel-sell": marketCancel(order); break;
  }
}
