module.exports = apiCalls;
var notify     = require("./notify");
var getOptions = require("./getOptions");
var getPath    = require("./getPath");
var makeRequest = require("./makeRequest");

// notify needs to be modified
function bittrex(call){
  notify(call);
  var options = getOptions(call);
  makeRequest(options);
}

function publicGetMarkets(){
  notify();
  var options = getOptions();
  options.path = getPath("getmarkets");
  makeRequest(options);
}

function publicGetMarketSummaries(){
  notify();
  var options = getOptions();
  options.path = getPath("getmarketsummaries");
  makeRequest(options);
}

function publicGetOrderbook(market){
  notify(market);
  var options = getOptions();
  options.path = "/api/v1.1/public/getorderbook?market="+market+"&type=both";
  makeRequest(options);
}

function accountGetBalance(currency){
  notify(currency);
  var options = getOptions();
  var path = "/api/v1.1/account/getbalance?apikey="+getApiKey("info")+"&currency="+currency+"&nonce="+getNonce();
  options.headers.apisign = hasher("https://bittrex.com"+path, getApiSecret("info"));
  options.path = path;
  makeRequest(options);
}

function accountGetBalances(){
  notify();
  var options = getOptions();
  var path  = "/api/v1.1/account/getbalances?apikey="+getApiKey("info")+"&nonce="+getNonce();
  options.headers.apisign = hasher("https://bittrex.com"+path, getApiSecret("info"));
  options.path = path;
  makeRequest(options);
}

function marketGetOpenOrders(){
  notify();
  var options = getOptions();
  var path  = "/api/v1.1/market/getopenorders?apikey="+getApiKey("info")+"&nonce="+getNonce();
  options.headers.apisign = hasher("https://bittrex.com"+path, getApiSecret("info"));
  options.path = path;
  makeRequest(options);
}

function publicGetTicker(market){
  notify(market);
  var options = getOptions();
  var path  = "/api/v1.1/public/getticker?market="+market;
  options.headers.apisign = "";
  options.path = path;
  makeRequest(options);
}

function marketCancel(uuid){
  notify(uuid);
  var options = getOptions();
  var path = "/api/v1.1/market/cancel?apikey="+getApiKey("limit")+"&uuid="+uuid+"&nonce="+getNonce();
  options.headers.apisign = hasher("https://bittrex.com"+path, getApiSecret("limit"));
  options.path = path;
  makeRequest(options);
}

function marketBuyLimit(market, quantity, rate){
  notify(market, quantity, rate);
  var options = getOptions();
  var path  = "/api/v1.1/market/buylimit?apikey="+getApiKey("limit")+"&market="+market+"&quantity="+quantity+"&rate="+rate+"&nonce="+getNonce();
  options.headers.apisign = hasher("https://bittrex.com"+path, getApiSecret("limit"));
  options.path = path;
  makeRequest(options);
}

function marketSellLimit(market, quantity, rate){
  notify(market, quantity, rate);
  var options = getOptions();
  var path  = "/api/v1.1/market/selllimit?apikey="+getApiKey("limit")+"&market="+market+"&quantity="+quantity+"&rate="+rate+"&nonce="+getNonce();
  options.headers.apisign = hasher("https://bittrex.com"+path, getApiSecret("limit"));
  options.path = path;
  makeRequest(options);
}

function accountGetOrderHistory(market){
  notify(market);
  var options = getOptions();
  var path  = "/api/v1.1/account/getorderhistory?apikey="+getApiKey("info")+"&market="+market+"&nonce="+getNonce();
  options.headers.apisign = hasher("https://bittrex.com"+path, getApiSecret("info"));
  options.path = path;
  makeRequest(options);
}
