module.exports = makeRequest;

var https = require("https");
var parse = require("./parse");

function makeRequest(options){
  var request = https.request(options);
  request.end();
  request.on("socket", function(socket){
    socket.on("close", function(hadError) { if(!hadError) { parse(); }});
    socket.on("data", function(buffer) { global.app.requestResult += buffer.toString(); });
    socket.on("error", function(error) { console.log("net.Socket Event 'error'", error); });
    socket.on("connect", function() { console.log("net.Socket Event 'connect'"); });
    socket.on("drain", function() { console.log("net.Socket Event 'drain'"); });
    socket.on("end", function() { console.log("net.Socket Event 'end'"); });
    socket.on("timeout", function() { console.log("net.Socket Event 'timeout'"); });
  });
}
