module.exports = notify;

function notify()
{
  var notificationMessage = "\n" + arguments.callee.caller.name;

  for(var i = 0, j = arguments.length; i < j; ++i)
  {
    if(isFirstIteration(i))
    {
      notificationMessage += "(";
    }

    notificationMessage += arguments[i];

    if(hasArguments(j) && isNotLastIteration(i, j))
    {
      notificationMessage += ", ";
    }

    if(isLastIteration(i, j))
    {
      notificationMessage += ")";
    }
  }

  console.log(notificationMessage);
}

function isFirstIteration(i)
{
  return i === 0;
}

function hasArguments(j)
{
  return j > 1;
}

function isLastIteration(i, j)
{
  return j - i === 1;
}

function isNotLastIteration(i, j)
{
  return !isLastIteration(i, j)
}