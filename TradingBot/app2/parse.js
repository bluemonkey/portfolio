module.export = parse;
var saveRequestResult = require("./saveRequestResult");

function parse() {
  var pattern = /(\r\n|\r|\n)[0-9a-f]{1,4}(\r\n|\r|\n){(.*(\r\n|\r|\n)?)*}/g;
  var match = global.app.requestResult.match(pattern);
  if(match === null){

    console.log(global.app.requestResult);
    console.log("request result from the server was null");
  }

  else if(match !== null){
    match = match[0];
    var startIndex = 0;
    var openingCRLFOffset = 2;
    var closingCRLFOffset = 2;
    var hexMaxLength = 4;
    var hexStart, hexEnd, hex, chunkLength, sliceStart, slice;
    var slices = [];

    while(1){
      hexStart = startIndex + openingCRLFOffset;
      hexEnd = hexStart + hexMaxLength;
      hex = getHex(hexStart, hexEnd, match);

      if(hex === "")
        break;

      chunkLength = parseInt(hex, 16);
      sliceStart = hexStart + hex.length + closingCRLFOffset;
      slice = match.substr(sliceStart, chunkLength);
      slices.push(slice);
      startIndex = sliceStart + chunkLength;
    }

    var parsable = "";

    slices.forEach(function(v) {
      parsable += v;
    });

    var parsed = JSON.parse(parsable);

    if(parsed.success){
      console.log("request succeeded");
    }

    else if(!parsed.success){
      console.log("request failed", parsed.message);
      return;
    }
    global.app.requestResult = parsed.result;
    saveRequestResult();
  }
}

function getHex(i, j, match){
  var hex = "";
  var char;
  for(; i < j; ++i){
    char = match[i];
    if(char === undefined) break;
    if(char.match(/[0-9a-f]/) === null) break;
    else hex += char;
  }
  return hex;
}
