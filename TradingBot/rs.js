// global access point
global.app = {};

// app specific modules
global.app.main = require("./app/main");
global.app.request = require("./app/request");

// program start
global.app.main();