// command line instructions:

/* global.app.interval is the minimum pause interval between sending requests to the server.

[HEALTY LOAD]
If the server is busy in a sense that it takes longer than the timeout to handle requests
but still in a timely fashion: the program will behave well and obediently waits for a response
after which it will send a new request as soon as the response has been received and processed.

[DEADLY LOAD]
If the server is busy in a sense it doesn't respond at all it will cause an internal node.js
error leading to a program termination. These kind of errors I do not know how to handle yet,
nor do I know how they propagate nor am I too concerned with them as they are moderately rare.
However they are disruptive for a fully automated trading solution. It has to be fixed some day.
Root cause however is a problem on the server side, most likely due to server overload, scaling
issues or DDOS attack or something similarily severe.

[HEAVY LOAD]
If the server is busy in a sense that it is still capable to respond with a timeout notification in html
form, the program terminates nicely. However it will dump the notification unparsed to the console.
The parser that parses the responses from server doesn't yet handle these scenarios. Consider fixing
this in the future as this can be rather common and disruptive if you are planning to automate trading
fully.

*/

// Interval in seconds.
global.app.interval = process.argv[2];

// Any action defined in the main function switch body.
global.app.action = process.argv[3];

// Any market supported by the bittrex api.
global.app.market = process.argv[4];

// Maximum price when placing a range of orders.
global.app.upper = process.argv[5];

// Minimum price when placing a range of orders.
global.app.lower = process.argv[6];

// Number of orders to be placed.
global.app.count = process.argv[7];

// Total amount to be used in either buying or selling (in bitcoin).
global.app.balance = process.argv[8];

// asynch control
global.app.phase = -1;

/* TODO: get rid of all global variables and implement a single function that works as an access point
   TODO: in updating variables that would otherwise had to be global variables. With this the code base
   TODO: could maybe look less cluttered.
* */

module.exports = main;

function main(){
  switch(global.app.action){
    case "buy": buyOrSell(); break;
    case "sell": buyOrSell(); break;
    case "cancel-buy": cancelBuyOrSell(); break;
    case "cancel-sell": cancelBuyOrSell(); break;
  }
}

function buyOrSell(){
  switch(++global.app.phase){
    case 0: prepareOrders(); break;
    case 1: executeOrder(); break;
    case 2: repeatPreviousLine(); break;
  }
}

function cancelBuyOrSell(){
  switch(++global.app.phase){
    case 0: waitCall(0, global.app.interval, global.app.request, "openOrders"); break;
    case 1: prepareOrders(); break;
    case 2: executeOrder(); break;
    case 3: repeatPreviousLine(); break;
  }
}

function executeOrder(){
  waitCall(0, global.app.interval, global.app.request, global.app.action, global.app.orders.splice(0,1)[0]);
}

function repeatPreviousLine(){
  if(global.app.orders.length !== 0){
    global.app.phase -= 2;
  }
  else {
    console.log("-done-");
  }
  main();
}

function prepareOrders(){
  var orders = [];

  switch(global.app.action){
    case "buy": buyOrSell(); break;
    case "sell": buyOrSell(); break;
    case "cancel-buy": cancelBuyOrSell(); break;
    case "cancel-sell": cancelBuyOrSell(); break;
  }

  function buyOrSell(){
    if(parseInt(global.app.count) < 2)
    {
      throw "bad count value", global.app.count;
    }

    var range = round(global.app.upper - global.app.lower, "up");
    var distance = round(range / (global.app.count-1), "down");
    var volume = round(global.app.balance / global.app.count, "down");
    var fee = 0.9975;
    var rounder = 100000000;

    for(var i = 0, j = global.app.count; i < j; ++i)
    {
      var rate = (global.app.upper * rounder - i * distance * rounder) / rounder;
      orders.push({
        market: global.app.market,
        quantity: global.app.action === "buy" ? round(volume / rate * fee, "down").toFixed(8) : volume,
        rate: rate.toFixed(8)
      });
    }

    console.log("\naction:"   +"'"+         global.app.action   +"'"+
                "\nmarket:"   +"'"+         global.app.market   +"'"+
                "\nupper:"    +"'"+  format(global.app.upper)   +"'"+
                "\nlower:"    +"'"+  format(global.app.lower)   +"'"+
                "\ncount:"    +"'"+         global.app.count    +"'"+
                "\nbalance:"  +"'"+  format(global.app.balance) +"'"+
                "\nrange:"    +"'"+  format(range)              +"'"+
                "\ndistance:" +"'"+  format(distance)           +"'"+
                "\nvolume:"   +"'"+  format(volume)             +"'"+
                "\n");

    orders.forEach(function(v) {
      console.log("market:"  +"'"+        v.market    +"', "+
                  "quantity:"+"'"+ format(v.quantity) +"', "+
                  "rate:"    +"'"+ format(v.rate)     +"'"+
                  "\n");
    });
    console.log("");
  }

  function cancelBuyOrSell(){
    if(global.app.action === "cancel-buy"){
      global.app.openOrders.forEach(function(v) {
        if(v.OrderType === "LIMIT_BUY" && v.Exchange === global.app.market){
          orders.push(v.OrderUuid);
        }
      });
    }
    else if(global.app.action === "cancel-sell"){
      global.app.openOrders.forEach(function(v) {
        if(v.OrderType === "LIMIT_SELL" && v.Exchange === global.app.market){
          orders.push(v.OrderUuid);
        }
      });
    }

  }
  global.app.orders = orders;
  main();
}

function round(number, dir){
  var rounder = 100000000;
  if(dir === "down"){
    return Math.floor(number * rounder) / rounder;
  }
  else if(dir === "up"){
    return Math.ceil(number * rounder) / rounder;
  }
}

function format(item){
  return parseFloat(item).toFixed(8);
}

function waitCall(start, seconds, callback, param1, param2){
  if(!start){
    start = Date.now();
    setTimeout(waitCall, 0, start, seconds, callback, param1, param2)
  }
  else if(start){
    if(Date.now() - start < seconds*1000){
      setTimeout(waitCall, 0, start, seconds, callback, param1, param2);
    }
    else {
      callback(param1, param2);
    }
  }
}
