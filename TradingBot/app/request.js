module.exports = request;

var hasher = require("../node_modules/crypto-js/hmac-sha512");
var fs = require("fs");
var https = require("https");
var requestResult = "";
var requestQuery = undefined;
var requestOrder = undefined;

function request(query, order){
  requestQuery = query;
  requestOrder = order;
  if(order !== undefined){
    if(typeof order === "object"){
      console.log("market: '"+order.market+"', quantity: '"+order.quantity+"', rate: '"+order.rate+"'");
    }
    else {
      console.log("uuid: '"+order+"'");
    }
  }
  switch(query){
    // experimental
    case "balances": accountGetBalances(); break;
    case "openOrders": marketGetOpenOrders(); break;
    case "markets": publicGetMarkets(); break;
    case "summaries": publicGetMarketSummaries(); break;
    case "orderbook": publicGetOrderbook(order); break;

    // core functions
    case "buy": marketBuyLimit(order.market, order.quantity, order.rate); break;
    case "sell": marketSellLimit(order.market, order.quantity, order.rate); break;
    case "cancel-buy": marketCancel(order); break;
    case "cancel-sell": marketCancel(order); break;
  }
}

function saveRequestResult(){
  switch(requestQuery){
    case "balances": global.app.balances = requestResult; break;
    case "openOrders": global.app.openOrders = requestResult; break;
    case "markets": global.app.markets = requestResult; break;
    case "summaries": global.app.summaries = requestResult; break;
    case "orderbook": global.app.orderbook = requestResult; break;
  }
  requestResult = "";
  global.app.main();
}

function saveTest(){
  console.log("saveTest");
  console.log("not decrementing phase");
  // --global.app.phase;
}

function publicGetMarkets() {
  console.log("\npublicGetMarkets");
  var options = getOptions();
  options.headers.apisign = "";
  options.path = "/api/v1.1/public/getmarkets";
  makeRequest(options);
}

function publicGetMarketSummaries(){
  console.log("\npublicGetMarketSummaries");
  var options = getOptions();
  options.headers.apisign = "";
  options.path = "/api/v1.1/public/getmarketsummaries";
  makeRequest(options);
}

function publicGetOrderbook(market){
  console.log("\npublicGetOrderbook");
  var options = getOptions();
  options.headers.apisign = "";
  options.path = "/api/v1.1/public/getorderbook?market="+market+"&type=both";
  makeRequest(options);
}

function accountGetBalance(currency){
  console.log("\naccountGetBalance('"+currency+"')");
  var options = getOptions();
  var path = "/api/v1.1/account/getbalance?apikey="+getApiKey("info")+"&currency="+currency+"&nonce="+getNonce();
  options.headers.apisign = hasher("https://bittrex.com"+path, getApiSecret("info"));
  options.path = path;
  makeRequest(options);
}

function accountGetBalances(){
  console.log("\naccountGetBalances");
  var options = getOptions();
  var path  = "/api/v1.1/account/getbalances?apikey="+getApiKey("info")+"&nonce="+getNonce();
  options.headers.apisign = hasher("https://bittrex.com"+path, getApiSecret("info"));
  options.path = path;
  makeRequest(options);
}

function marketGetOpenOrders(){
  console.log("\nmarketGetOpenOrders");
  var options = getOptions();
  var path  = "/api/v1.1/market/getopenorders?apikey="+getApiKey("info")+"&nonce="+getNonce();
  options.headers.apisign = hasher("https://bittrex.com"+path, getApiSecret("info"));
  options.path = path;
  makeRequest(options);
}

function publicGetTicker(market){
  console.log("publicGetTicker");
  var options = getOptions();
  var path  = "/api/v1.1/public/getticker?market="+market;
  options.headers.apisign = "";
  options.path = path;
  makeRequest(options);
}

function marketCancel(uuid){
  console.log("marketCancel");
  var options = getOptions();
  var path = "/api/v1.1/market/cancel?apikey="+getApiKey("limit")+"&uuid="+uuid+"&nonce="+getNonce();
  options.headers.apisign = hasher("https://bittrex.com"+path, getApiSecret("limit"));
  options.path = path;
  makeRequest(options);
}

function marketBuyLimit(market, quantity, rate){
  console.log("marketBuyLimit");
  var options = getOptions();
  var path  = "/api/v1.1/market/buylimit?apikey="+getApiKey("limit")+"&market="+market+"&quantity="+quantity+"&rate="+rate+"&nonce="+getNonce();
  options.headers.apisign = hasher("https://bittrex.com"+path, getApiSecret("limit"));
  options.path = path;
  makeRequest(options);
}

function marketSellLimit(market, quantity, rate){
  console.log("marketSellLimit");
  var options = getOptions();
  var path  = "/api/v1.1/market/selllimit?apikey="+getApiKey("limit")+"&market="+market+"&quantity="+quantity+"&rate="+rate+"&nonce="+getNonce();
  options.headers.apisign = hasher("https://bittrex.com"+path, getApiSecret("limit"));
  options.path = path;
  makeRequest(options);
}

function accountGetOrderHistory(market){
  console.log("accountGetOrderHistory");
  var options = getOptions();
  var path  = "/api/v1.1/account/getorderhistory?apikey="+getApiKey("info")+"&market="+market+"&nonce="+getNonce();
  options.headers.apisign = hasher("https://bittrex.com"+path, getApiSecret("info"));
  options.path = path;
  makeRequest(options);
}

function getOptions() {
  return {
    headers: {
      "apisign": "",
      "Content-type": "application/json; charset=utf-8"
    },
    hostname: "bittrex.com",
    method: "GET",
    path: undefined,
    port: 443,
    protocol: "https:"
  };
}

function getApiKey(query) {
  var options = { encoding: "utf8", flag: "r" };
  switch(query) {
    case "info":
      return fs.readFileSync("./keys/info.txt", options);
    case "limit":
      return fs.readFileSync("./keys/limit.txt", options);
  }
}

function getNonce() {
  return Math.floor(Date.now() / 1000);
}

function getApiSecret(query) {
  var options = { encoding: "utf8", flag: "r" };
  switch(query) {
    case "info":
      return fs.readFileSync("./secrets/info.txt", options);
    case "limit":
      return fs.readFileSync("./secrets/limit.txt", options);
  }
}

function makeRequest(options){
  var request = https.request(options);

  request.end();

  /*
  request.on("abort", function() { console.log("https.ClientRequest Event 'abort'"); });
  request.on("aborted", function() { console.log("https.ClientRequest Event 'aborted'");});
  */

  request.on("socket", function(socket) { //console.log("https:ClientRequest Event 'socket'");
    socket.on("close", function(hadError) { //console.log("net.Socket Event 'close' hadError", hadError);
      if(!hadError) { parse(); }
    });
    socket.on("data", function(buffer) { requestResult += buffer.toString(); });
    socket.on("error", function(error) { console.log("net.Socket Event 'error'", error); });
    socket.on("connect", function() { console.log("net.Socket Event 'connect'"); });
    socket.on("drain", function() { console.log("net.Socket Event 'drain'"); });
    socket.on("end", function() { console.log("net.Socket Event 'end'"); });
    socket.on("timeout", function() { console.log("net.Socket Event 'timeout'"); });


  });
}

function parse() {
  var pattern = /(\r\n|\r|\n)[0-9a-f]{1,4}(\r\n|\r|\n){(.*(\r\n|\r|\n)?)*}/g;
  var match = requestResult.match(pattern);
  if(match === null){

    console.log(requestResult);
    console.log("request result from the server was null");
  }

  else if(match !== null){
    match = match[0];
    var startIndex = 0;
    var openingCRLFOffset = 2;
    var closingCRLFOffset = 2;
    var hexMaxLength = 4;
    var hexStart, hexEnd, hex, chunkLength, sliceStart, slice;
    var slices = [];

    while(1){
      hexStart = startIndex + openingCRLFOffset;
      hexEnd = hexStart + hexMaxLength;
      hex = getHex(hexStart, hexEnd, match);

      if(hex === "")
        break;

      chunkLength = parseInt(hex, 16);
      sliceStart = hexStart + hex.length + closingCRLFOffset;
      slice = match.substr(sliceStart, chunkLength);
      slices.push(slice);
      startIndex = sliceStart + chunkLength;
    }

    var parsable = "";

    slices.forEach(function(v) {
      parsable += v;
    });

    var parsed = JSON.parse(parsable);

    if(parsed.success){
      console.log("request succeeded");
    }

    else if(!parsed.success){
      console.log("request failed", parsed.message);
      return;
    }
    requestResult = parsed.result;
    saveRequestResult();
  }
}

function getHex(i, j, match){
  var hex = "";
  var char;
  for(; i < j; ++i){
    char = match[i];
    if(char === undefined) break;
    if(char.match(/[0-9a-f]/) === null) break;
    else hex += char;
  }
  return hex;
}